import React from 'react';

const Footer = () => {
  return (
    <footer className="page-footer font-small blue footer">
      <div className="footer-copyright text-center py-3">
        <a href="" target="_blank" rel="noopener noreferrer" className="author">
          About us
        </a>
      </div>
    </footer>
  );
};

export default Footer;

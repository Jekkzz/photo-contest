import React, { useContext, Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../../Context/AuthContext';
import { removeUserSession, getUser } from '../../Utils/Common';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import { withSnackbar } from 'notistack';
import { withRouter } from 'react-router-dom';

const Nav = (props) => {
  const { isLoggedIn, setLoginState } = useContext(AuthContext);
  const user = getUser();
  const history = useHistory();

  const handleLogout = () => {
    removeUserSession();
    setLoginState(false);
    history.push('/login');
    props.enqueueSnackbar('You logged out successfully', { variant: 'success' });
  };

  const drawerWidth = 240;
  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
    },
    appBar: {
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
  }));

  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="absolute"
        color="primary"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          {!isLoggedIn ? (
            <Fragment>
              <Typography variant="h6" noWrap>
                <Link href="/" color="inherit">
                  Photo Contests
                </Link>
              </Typography>
              <Button
                variant="outlined"
                color="inherit"
                href="/login"
                style={{ position: 'absolute', right: '5%' }}
              >
                Login
              </Button>
            </Fragment>
          ) : (
            <Fragment>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" noWrap>
                <Link href="/dashboard" color="inherit">
                  Photo Contests
                </Link>
              </Typography>
              <Button onClick={handleLogout} style={{ position: 'absolute', right: '5%' }}>
                Logout
              </Button>
            </Fragment>
          )}
        </Toolbar>
      </AppBar>
      {isLoggedIn ? (
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </div>
          <Divider />
          {user.role === 'Organizer' ? (
            <Fragment>
              <List>
                <ListItem button key={'Create Contest'}>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  <Link href="/create" color="inherit">
                    Create Contest
                  </Link>
                  <ListItemText />
                </ListItem>
                <ListItem button key={'Contests'}>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  <Link href="/dashboard/contests" color="inherit">
                    All Contests
                  </Link>
                  <ListItemText />
                </ListItem>
              </List>
              <Divider />
              <List>
                <ListItem button key={'Users'} onClick={() => props.history.push('/dashboard')}>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  Users
                  <ListItemText />
                </ListItem>
              </List>
            </Fragment>
          ) : (
            <Fragment>
              <List>
                <ListItem
                  button
                  key={'Active Contests'}
                  onClick={() => history.push(`/contests/open`)}
                >
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  Active Contests
                  <ListItemText />
                </ListItem>
                <ListItem
                  button
                  key={'Currently Participating Contests'}
                  onClick={() => history.push(`/contests/user`)}
                >
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  Currently Participating Contests
                  <ListItemText />
                </ListItem>
              </List>
              <Divider />
              <List>
                <ListItem
                  button
                  key={'Finished Contests'}
                  onClick={() => history.push(`/contests/finished`)}
                >
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  Finished Contests
                  <ListItemText />
                </ListItem>
              </List>
            </Fragment>
          )}
        </Drawer>
      ) : null}
    </div>
  );
};

export default withSnackbar(withRouter(Nav));

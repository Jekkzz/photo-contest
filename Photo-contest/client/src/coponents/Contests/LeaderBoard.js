import React, { useEffect, useState } from 'react';
import httpProvider from '../../providers/http-provider';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 300,
  },
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
    textAlign: 'center',
    fontSize: 30,
  },
}));

const LeaderBoard = (props) => {
  const classes = useStyles();
  const [dense, setDense] = React.useState(false);
  const [secondary, setSecondary] = React.useState(false);
  const [leaderboard, setLeaderboard] = useState([]);
  const [rows, setRows] = useState([]);

  const createData = (place, average, username) => {
    return { place, average, username };
  };

  useEffect(() => {
    if (leaderboard.length) {
      setRows([
        createData(
          leaderboard[0] ? leaderboard[0].place : 'None',
          leaderboard[0] ? leaderboard[0].average : 'None',
          leaderboard[0] ? leaderboard[0].username : 'None',
        ),
        createData(
          leaderboard[1] ? leaderboard[1].place : 'None',
          leaderboard[1] ? leaderboard[1].average : 'None',
          leaderboard[1] ? leaderboard[1].username : 'None',
        ),
        createData(
          leaderboard[2] ? leaderboard[2].place : 'None',
          leaderboard[2] ? leaderboard[2].average : 'None',
          leaderboard[2] ? leaderboard[2].username : 'None',
        ),
      ]);
    }
  }, [leaderboard]);

  useEffect(() => {
    httpProvider.get(`http://localhost:3000/contests/${props.contest_id}/points`).then((data) => {
      setLeaderboard(data);
      console.log(data);
    });
  }, []);

  return (
    <Grid container spacing={2} style={{ marginBottom: '100px' }}>
      <Grid item xs={12} md={6} style={{ margin: 'auto' }}>
        <Typography variant="h6" className={classes.title}>
          Leaderboard
        </Typography>
        <div className={classes.demo}>
          {leaderboard.length && (
            <TableContainer component={Paper} style={{ marginBottom: 15 }}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>
                      <strong>Place</strong>
                    </TableCell>
                    <TableCell>
                      <strong>Score</strong>
                    </TableCell>
                    <TableCell>
                      <strong>Username</strong>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row, index) => (
                    <TableRow key={index}>
                      <TableCell component="th" scope="row">
                        {row.place}
                      </TableCell>
                      <TableCell>{row.average}</TableCell>
                      <TableCell>{row.username}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </div>
      </Grid>
    </Grid>
  );
};
export default LeaderBoard;

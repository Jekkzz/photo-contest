import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    transition: '0.5s all ease-in-out',
    '&:hover': {
      opacity: '1',
      transform: 'scale(1.1)',
      cursor: 'pointer',
    },
  },
  media: {
    height: 180,
  },
  button: {
    marginTop: theme.spacing(0),
    fontFamily: '"Segoe UI"',
    fontSize: '12px',
    fontWeight: '1px',
  },
  text: {
    color: 'black',
    fontSize: '20px',
    fontFamily: '"Segoe UI"',
    fontWeight: '500',
  },
  title: {
    fontSize: '50px',
  },
}));

const ContestDetails = (props) => {
  const isUserJury = props.jury.includes(props.userData.sub);
  const classes = useStyles();

  const handleClick = (data) => {
    props.history.push({
      pathname: `contests/${props.contest_id}`,
    });
  };

  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card className={classes.card} onClick={() => handleClick(props)}>
        <CardContent>
          <Typography className={classes.title} variant="h1">
            {props.title}
          </Typography>
          <Typography className={classes.pos}>Category: {props.category}</Typography>
          <Typography variant="body2" component="p"></Typography>
        </CardContent>
        <CardActions>
          {isUserJury || props.status === 'Finished' ? (
            ''
          ) : (
            <Button
              disabled={props.user_id === props.userData.sub}
              onClick={() => props.joinTheContest(props.contest_id)}
              size="small"
              style={{ color: 'white' }}
            >
              {props.user_id !== props.userData.sub ? 'Join' : 'You are in!'}
            </Button>
          )}
        </CardActions>
      </Card>
    </Grid>
  );
};

export default withRouter(ContestDetails);

import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import httpProvider from '../../providers/http-provider';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    transition: '0.5s all ease-in-out',
    '&:hover': {
      opacity: '1',
      transform: 'scale(1.1)',
      cursor: 'pointer',
    },
  },
  media: {
    height: 180,
  },
  button: {
    marginTop: theme.spacing(0),
    fontFamily: '"Segoe UI"',
    fontSize: '12px',
    fontWeight: '1px',
  },
  text: {
    color: 'black',
    fontSize: '15px',
    fontFamily: '"Segoe UI"',
    fontWeight: '500',
  },
  title: {
    fontSize: '50px',
  },
}));

const Contests = (props) => {
  const classes = useStyles();
  const [phasetime, setPhasetime] = useState([]);

  useEffect(() => {
    httpProvider
      .get(`http://localhost:3000/contests/${props.contest_id}/timelimit`)
      .then((data) => setPhasetime(data));
  }, [props.contests_id]);

  const handleClick = () => {
    props.history.push({
      pathname: `/contests/contests/${props.contest_id}`,
    });
  };

  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card className={classes.card} onClick={() => handleClick(props)}>
        <CardContent>
          <Typography className={classes.title} variant="h1">
            {props.title}
          </Typography>
          <Typography className={classes.pos}>Category: {props.category}</Typography>
          {phasetime === null ? (
            <Typography className={classes.pos}>Finished</Typography>
          ) : (
            <Typography className={classes.pos}>
              Days:{' '}
              {phasetime.daysUntilPhase2 || phasetime.daysUntilPhase2 === 0
                ? phasetime.daysUntilPhase2
                : phasetime.daysUntilFinished}
              <br />
              Hours:{' '}
              {phasetime.timeUntilPhase2 || phasetime.timeUntilPhase2 === 0
                ? phasetime.timeUntilPhase2
                : phasetime.timeUntilFinished}
              <br />
              Minutes:{' '}
              {phasetime.minutesUntilPhase2 || phasetime.minutesUntilPhase2 === 0
                ? phasetime.minutesUntilPhase2
                : phasetime.minutesUntilFinished}
            </Typography>
          )}
          <Typography variant="body2" component="p"></Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};
export default withRouter(Contests);

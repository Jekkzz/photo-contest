import React, { useEffect, useState, Fragment } from 'react';
import Loader from '../Loader/Loader';
import ContestDetails from './ContestDetails';
import httpProvider from '../../providers/http-provider';
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { getUser } from '../../Utils/Common';

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(10),
    paddingBottom: theme.spacing(10),
    display: 'flex',
  },
}));

const Contests = () => {
  const classes = useStyles();
  const params = useParams();
  const [contests, setContests] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState('');

  useEffect(() => {
    const userData = getUser();
    setUser(userData);
  }, []);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/contests/contests/${params.type}`)
      .then((data) => setContests(data))
      .catch((error) => {
        setError(error.message);
      })
      .finally(() => setLoading(false));
  }, [params]);

  const joinContest = (id) => {
    httpProvider
      .post(`http://localhost:3000/contests/${id}`)
      .then((updatedContest) => {
        const index = contests.findIndex((contest) => contest.id === updatedContest.id);
        const copy = [...contests];
        copy[index] = updatedContest;

        setContests(copy);
      })
      .catch((err) => console.log(err));
  };

  if (error) {
    console.log(error);
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  if (loading) {
    return <Loader />;
  }

  const transformedContests =
    contests &&
    contests.length &&
    contests.map((contest, index) => {
      return (
        <ContestDetails key={index} {...contest} joinTheContest={joinContest} userData={user} />
      );
    });

  return (
    <Fragment>
      <Container style={{ marginTop: '70px' }}>
        <Grid container spacing={8} className={classes.cardGrid} maxwidth="md">
          {contests.length ? <>{transformedContests}</> : <div>No contests to show ...</div>}
        </Grid>
      </Container>
    </Fragment>
  );
};

export default Contests;

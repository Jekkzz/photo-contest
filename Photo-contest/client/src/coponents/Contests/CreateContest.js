import React, { useState, useEffect, Fragment } from 'react';
import httpProvider from '../../providers/http-provider';
import CircularIndeterminate from '../Loader/Loader';
import { useForm, Controller } from 'react-hook-form';
import Select from 'react-select';
import { withRouter } from 'react-router-dom';
import { withSnackbar } from 'notistack';
import './CreateContest.css';

const CreateContest = (props) => {
  const [users, setUsers] = useState([]);
  const [jury, setJury] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    httpProvider.get(`http://localhost:3000/register`).then((data) => setUsers(data));
  }, []);

  useEffect(() => {
    httpProvider.get(`http://localhost:3000/register/jury`).then((data) => setJury(data));
  }, []);

  const { watch, control, register, handleSubmit, getValues, errors } = useForm({
    mode: 'onBlur',
  });

  const newOptions = users.map((data, index) => ({
    label: data.username,
    value: data.user_id,
  }));

  let newJury;
  if (jury.length) {
    newJury = jury.map((data, index) => ({
      label: data.username,
      value: data.user_id,
    }));
  }

  const watchShowType = watch('type', 'Open');

  const onSubmit = () => {
    const values = getValues();
    setLoading(true);
    
    httpProvider.post(`http://localhost:3000/contests`, values).then((data) => {
      if (data.message) {
        props.enqueueSnackbar(`${data.message}`, { variant: 'error' });
        setLoading(false);
        return;
      } else setLoading(false);
      props.enqueueSnackbar('Contest created successfully', { variant: 'success' });
      props.history.push({
        pathname: `/dashboard/contests`,
      });
    });
  };

  if (loading) {
    return <CircularIndeterminate />;
  }

  return (
    <div className="Page" style={{ marginTop: '150px' }}>
      <div className="Create">
        <form className="Form" onSubmit={handleSubmit(onSubmit)}>
          <h1 className="login-title text-center">Create Contest</h1>
          <label className="label" htmlFor="title">
            Contest Title
          </label>
          <input
            type="text"
            placeholder="Title"
            name="title"
            ref={register({ required: true, maxLength: 80 })}
          />
          {errors.title && <p className="para">Contest title is required</p>}
          <label className="label" htmlFor="category">
            Contest Category
          </label>
          <input
            type="text"
            placeholder="Category"
            name="category"
            ref={register({ required: true, maxLength: 100 })}
          />
          {errors.category && <p className="para">Contest category is required</p>}
          <label className="label" htmlFor="type">
            Contest Type
          </label>
          <select className="select" name="type" ref={register({ required: true })}>
            <option value="Open">Open</option>
            <option value="Invitational">Invitational</option>
          </select>
          <label className="label" htmlFor="phase1">
            Phase 1 End Date
          </label>
          <input
            type="datetime"
            placeholder="1-30 days"
            name="phase1"
            ref={register({ required: true, pattern: /\b([1-9]|[12][0-9]|3[0])\b/i })}
          />
          {errors.phase1 && <p className="para">Phase 1 time should be between 1-30 days</p>}
          <label className="label" htmlFor="phase2">
            Phase 2 End Date
          </label>
          <input
            type="datetime"
            placeholder="1-24 hours"
            name="phase2"
            ref={register({ required: true, pattern: /\b([1-9]|1[0-9]|2[0-4])\b/i })}
          />
          {errors.phase2 && <p className="para">Phase 2 time should be between 1-24 hours</p>}
          {watchShowType === 'Invitational' ? (
            <Fragment>
              <label className="label" htmlFor="invite">
                Invite Users
              </label>
              <Controller
                defaultValue={'Invite'}
                name="invite"
                as={Select}
                isMulti
                options={newOptions}
                className="basic-multi-select"
                classNamePrefix="select"
                control={control}
                rules={{ required: true }}
              />
              {errors.invite && <p className="para">You need to select users</p>}
            </Fragment>
          ) : null}
          <label className="label" htmlFor="jury">
            Select Additional Jury
          </label>
          <Controller
            defaultValue="Jury"
            name="jury"
            as={Select}
            isMulti
            options={newJury}
            className="basic-multi-select"
            classNamePrefix="select"
            control={control}
          />
          <input type="submit" />
        </form>
      </div>
    </div>
  );
};

export default withSnackbar(withRouter(CreateContest));

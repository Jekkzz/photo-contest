import React from 'react';
import httpProvider from '../../providers/http-provider';
import { useForm } from 'react-hook-form';
import { withRouter } from 'react-router-dom';
import './PhotoReview.css';
import { withSnackbar } from 'notistack';

const PhotoReview = (props) => {

  const photo_id = props.match.params.pathParam2;
  const contest_id = props.match.params.id;
  
  const { watch, control, register, handleSubmit, getValues, errors } = useForm({
    mode: 'all',
  });

  const onSubmit = (ev) => {
    const values = getValues();
    httpProvider.post(
      `http://localhost:3000/contests/${contest_id}/photos/${photo_id}/reviews`,
      values,
    );
    props.enqueueSnackbar('Review created successfully', { variant: 'success' });
    props.history.goBack();
  };

  return (
    <div className="Page" style={{ marginTop: '150px' }}>
      <div className="Create">
        <form className="Form" onSubmit={handleSubmit(onSubmit)}>
          <h1 className="login-title text-center">Photo review</h1>
          <label className="label" htmlFor="comment">
            Photo Comment
          </label>
          <textarea
            className="input"
            type="text-area"
            placeholder="Photo Comment"
            name="comment"
            ref={register({ required: true, maxLength: 250, minLength: 25 })}
          />
          {errors.comment && (
            <p className="para">Photo comment is required and to be between 25-250 symbols</p>
          )}
          <label
            className="label"
            htmlFor="score"
            style={{ postion: 'absolute', display: 'contents' }}
          >
            Photo Score:
          </label>
          <select
            name="score"
            style={{ marginLeft: '15px', transform: 'scale(1.1)' }}
            ref={register({ required: true })}
          >
            <option value={+1}>1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
          </select>
          {errors.score && <p className="para">Photo score is required</p>}
          <label className="label" htmlFor="wrongCategory" style={{ textAlign: 'center' }}>
            Wrong Category
            <input
              type="checkbox"
              style={{ width: '50%' }}
              style={{ transform: 'scale(1.5)' }}
              placeholder="wrongCategory"
              name="wrongCategory"
              ref={register}
            />
          </label>
          <input type="submit" />
        </form>
      </div>
    </div>
  );
};

export default withSnackbar(withRouter(PhotoReview));

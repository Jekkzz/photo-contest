import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withRouter } from 'react-router-dom';
const useStyles = makeStyles({
  root: {
    minWidth: 275,
    maxWidth: 150,
    marginTop: 150,
    marginLeft: 500,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const UserRank = (props) => {
  const classes = useStyles();
  const { data } = props;
  const handleClick = () => {
    props.history.push('/contests/open');
  };
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          {data.username}
        </Typography>
        <Typography variant="h5" component="h2">
          Rank: {data.userrank}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          Current Points: {data.points}
        </Typography>
        <Typography variant="body2" component="p">
          Points until rank {data.nextrank}: {data.treshhold}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={() => handleClick()}>
          Join Contests
        </Button>
      </CardActions>
    </Card>
  );
};

export default withRouter(UserRank);

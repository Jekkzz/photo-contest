import React, { useState } from 'react';
import { getToken } from '../../Utils/Common';
import CircularIndeterminate from '../Loader/Loader';
import { withRouter } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { withSnackbar } from 'notistack';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';

function PaperComponent(props) {
  return <Paper {...props} />;
}
const UploadPhoto = (props) => {
  
  const contestId = props.match.params.id;
  const [open, setOpen] = React.useState(false);
  const [file, setFile] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const { register, handleSubmit, getValues, errors } = useForm({
    mode: 'onBlur',
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const onSubmit = (data) => {
    const values = getValues();

    const formData = new FormData();
    formData.set('avatar', file);
    formData.set('photo_story', values.photo_story);
    formData.set('photo_title', values.photo_title);

    if (file) {
      setLoading(true);
      fetch(`http://localhost:3000/photos/${contestId}/picture`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${getToken()}`,
        },
        body: formData,
      })
        .then((res) => res.json())
        .catch((error) => {
          setError(error);
        })
        .then((data) => {
          console.log(data);
        })
        .finally(() => setLoading(false));
    }
    props.enqueueSnackbar('Photo uploaded successfully', { variant: 'success' });
    props.history.push('/contests/user');
  };

  if (loading) {
    return <CircularIndeterminate />;
  }

  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Warning!!!!
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to submit your photo Your entry can't be changed later!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit(onSubmit)} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
      <div className="Page">
        <div className="Create">
          <form className="Form" onSubmit={handleSubmit(handleClickOpen)}>
            <h1 className="login-title text-center" style={{ marginTop: '150px' }}>
              Upload Photo
            </h1>
            <div>
              <label className="label" htmlFor="photo_story">
                Photo Story
              </label>
              <textarea
                className="input"
                type="text-area"
                placeholder="Photo Story"
                name="photo_story"
                ref={register({ required: true, maxLength: 250, minLength: 25 })}
              />
              {errors.photo_story && (
                <p className="para">Photo Story is required and should be between 25-250 symbols</p>
              )}
            </div>
            <div>
              <label className="label" htmlFor="photo_title">
                Photo Title
              </label>
              <input
                type="text"
                placeholder="Photo Title"
                name="photo_title"
                ref={register({ required: true, max: 25, min: 3, minLength: 3, maxLength: 25 })}
              />
              {errors.photo_title && <p className="para">Photo Title is required</p>}
            </div>
            <div>
              <input
                ref={register}
                name="avatar"
                type="file"
                onChange={(e) => setFile(e.target.files[0])}
              />
            </div>
            <input type="submit" />
          </form>
        </div>
      </div>
    </>
  );
};

export default withSnackbar(withRouter(UploadPhoto));

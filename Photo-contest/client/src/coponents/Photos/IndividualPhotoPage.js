import React, { useEffect, useState } from 'react';
import httpProvider from '../../providers/http-provider';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '50%',
    margin: 'auto',
    marginTop: 100,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  fab: {
    margin: theme.spacing(2),
  },
  absolute: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(3),
  },
  table: {
    width: 1100,
    margin: 'auto',
  },
}));

const IndividualPhotoPage = (props) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const photo_id = props.match.params.pathParam2;
  const [reviews, setReviews] = useState([]);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    httpProvider
      .get(`http://localhost:3000/photos/${photo_id}/reviews`)
      .then((data) => setReviews(data));
  }, []);

  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

  return (
    <>
      {reviews?.length ? (
        <Card className={classes.root}>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                {reviews[0].username[0]}
              </Avatar>
            }
            title={reviews[0].photo_title}
            subheader={`by: ${reviews[0].username}`}
          />
          <CardMedia
            className={classes.media}
            image={`http://localhost:3000/public/${reviews[0].photo_path}`}
            title={reviews[0].photo_title}
          />
          <CardContent></CardContent>
          <Tooltip title="Photo Story" placement="bottom-end">
            <CardActions disableSpacing>
              <IconButton
                className={clsx(classes.expand, {
                  [classes.expandOpen]: expanded,
                })}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
              >
                <ExpandMoreIcon />
              </IconButton>
            </CardActions>
          </Tooltip>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <CardContent>
              <Typography paragraph>Photo Story:</Typography>
              <Typography paragraph>{reviews[0].photo_story}</Typography>
            </CardContent>
          </Collapse>
        </Card>
      ) : null}
      <div style={{ margin: 'auto', marginTop: '100px', width: 'fit-content' }}>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Jury</StyledTableCell>
                <StyledTableCell align="right">Comment</StyledTableCell>
                <StyledTableCell align="right">Score</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {reviews.map((review) => (
                <StyledTableRow key={review.jury}>
                  <StyledTableCell component="th" scope="row">
                    {review.jury}
                  </StyledTableCell>
                  <StyledTableCell align="right">{review.photo_comment}</StyledTableCell>
                  <StyledTableCell align="right">{review.photo_score}</StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </>
  );
};

export default IndividualPhotoPage;

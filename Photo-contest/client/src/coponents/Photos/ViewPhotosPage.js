import React from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import GridList from '@material-ui/core/GridList';
import { getUser } from '../../Utils/Common';
import LeaderBoard from '../Contests/LeaderBoard';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import { red } from '@material-ui/core/colors';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme) => ({
  root1: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
    marginBottom: 100,
    margin: 20,
  },
  card: {
    width: '45%',
    margin: 30,
  },
  gridList: {
    width: '100%',
    height: '100%',
    marginBottom: 150,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  avatar: {
    backgroundColor: red[500],
  },
  fab: {
    margin: theme.spacing(2),
  },
  absolute: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(3),
  },
  tile: {
    height: '100%',
  },
}));

const ViewPhotos = (props) => {
  const { photos, contest, photo } = props;
  const user = getUser();
  const classes = useStyles();
  const isJury = contest.jury.some((jury) => jury.user_id === user.sub);

  const handleClick = (id) => {
    if (isJury) {
      props.history.push(`${contest.contest.contest_id}/photos/${id}/review`, contest);
    } else {
      props.history.push(`${contest.contest.contest_id}/photos/${id}`, contest);
    }
  };
  return (
    <>
      <>
        {photo ? (
          <>
            <h1 style={{ marginTop: '100px' }}>Your Photo</h1>
            <div className={classes.root1} style={{ marginInline: 'auto' }}>
              <Card className={classes.card} style={{ width: '45%' }}>
                <CardHeader
                  avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                      {photo.username}
                    </Avatar>
                  }
                  title={photo.photo_title}
                  subheader={`by: ${photo.username}`}
                />
                <CardMedia
                  className={classes.media}
                  image={`http://localhost:3000/public/${photo.photo_path}`}
                  title={photo.photo_title}
                />
                <CardContent></CardContent>
                <Tooltip title="Photo info" placement="bottom-start">
                  <CardActions disableSpacing>
                    <IconButton onClick={() => handleClick(photo.photo_id)} aria-label="show more">
                      <InfoIcon />
                    </IconButton>
                  </CardActions>
                </Tooltip>
              </Card>
            </div>
            <div>
              <LeaderBoard contest_id={contest.contest.contest_id} />
            </div>
          </>
        ) : null}
      </>
      <>
        {photos?.length ? (
          <>
            <h1>Other contestans photos</h1>
            <div className={classes.root1}>
              <GridList cellHeight={'auto'} spacing={15} cols={2} className={classes.gridList}>
                {photos.map((photo) => (
                  <Card className={classes.card} style={{ width: '45%' }}>
                    <CardHeader
                      avatar={
                        <Avatar aria-label="recipe" className={classes.avatar}>
                          {photo.username[0]}
                        </Avatar>
                      }
                      title={photo.photo_title}
                      subheader={`by: ${photo.username}`}
                    />
                    <CardMedia
                      className={classes.media}
                      image={`http://localhost:3000/public/${photo.photo_path}`}
                      title={photo.photo_title}
                    />
                    <CardContent></CardContent>
                    <Tooltip title="Photo info" placement="bottom-start">
                      <CardActions disableSpacing>
                        <IconButton
                          onClick={() => handleClick(photo.photo_id)}
                          aria-label="show more"
                        >
                          <InfoIcon />
                        </IconButton>
                      </CardActions>
                    </Tooltip>
                  </Card>
                ))}
              </GridList>
            </div>
          </>
        ) : (
          <h1 style={{ marginTop: '150px' }}>No Photos for this contest yet</h1>
        )}
      </>
    </>
  );
};

export default withRouter(ViewPhotos);

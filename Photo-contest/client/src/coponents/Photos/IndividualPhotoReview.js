import React, { useState, useEffect } from 'react';
import httpProvider from '../../providers/http-provider';
import PhotoReview from '../Review/PhotoReviewPage';

const IndividualPhotoReview = (props) => {

  const photo_id = props.match.params.pathParam2;
  const [review, setReview] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const contest = props.location.state;

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/photos/${photo_id}/review`)
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
      })
      .then((data) => setReview(data))
      .catch((error) => setError(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <>
      {contest.contest.status === 'Phase1' ? (
        <h1 style={{ marginTop: '100px' }}>
          You cannot give review while contest is still in Phase1
        </h1>
      ) : (
        <>
          {contest.contest.status === 'Finished' ? (
            <h1 style={{ marginTop: '100px' }}>Contest has already finished</h1>
          ) : (
            <>
              {error && contest.contest.status === 'Phase2' ? (
                <PhotoReview />
              ) : (
                <div>
                  <h1 style={{ marginTop: '100px' }}>You have already reviewed this photo</h1>
                </div>
              )}
            </>
          )}
        </>
      )}
    </>
  );
};

export default IndividualPhotoReview;

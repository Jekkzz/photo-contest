import React from 'react';

const Content = () => {
  return (
    <div>
      <div className="container content">
        <div className="row">
          <div className="col-sm-3 talk">
            <h1>Join</h1>
            <h1>Contests</h1>
            <br />
            <h6 className="bold-four">
              Compete against other people by uploading photos and getting ranked
            </h6>
            <br />
            <h6>
              <a className="btn btn-dark start start-two" href="/register">
                Get Started
              </a>
            </h6>
          </div>
          <div className="col-sm-9 showcase-img">{/* <div className="circle"></div> */}</div>
        </div>
      </div>

      <section
        className="features-icons text-center det-ails"
        style={{ backgroundColor: '#f1f1f1' }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-4">
              <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                <div className="features-icons-icon d-flex  icon-bra-ails">
                  <i className="icon-screen-desktop m-auto text-primary icon-ails"></i>
                </div>
                <h5>Join Contest</h5>
                <p className="lead mb-0">Join open contests</p>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                <div className="features-icons-icon d-flex  icon-bra-ails">
                  <i className="icon-layers m-auto text-primary icon-ails"></i>
                </div>
                <h5>Jury</h5>
                <p className="lead mb-0">Jury rates your photos and comments them.</p>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="features-icons-item mx-auto mb-0 mb-lg-3">
                <div className="features-icons-icon d-flex  icon-bra-ails">
                  <i className="icon-check m-auto text-primary icon-ails"></i>
                </div>
                <h5>Get points</h5>
                <p className="lead mb-0">
                  Get points depending on final ranking.See other contestants photos and points
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Content;

import { getToken } from '../Utils/Common';
const get = (url) => {
  const token = getToken();

  return fetch(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());
};

const post = (url, body) => {
  const token = getToken();

  return fetch(url, {
    body: JSON.stringify(body),
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
  }).then((res) => res.json());
};

const put = (url, body) => {
  const token = getToken();

  return fetch(url, {
    body: JSON.stringify(body),
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
  }).then((res) => res.json());
};

const remove = (url) => {
  const token = getToken();

  return fetch(url, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());
};

const httpProvider = {
  get,
  post,
  put,
  remove,
};

export default httpProvider;

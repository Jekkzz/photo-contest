import './Register.css';
import React from 'react';
import { useForm } from 'react-hook-form';
import { withSnackbar } from 'notistack';

const Register = (props) =>{
  
  const { register, handleSubmit, getValues,  errors } = useForm({
    mode: "onBlur"
  });
  
  const values = getValues();
  const onSubmit = event => {
  
  fetch(`http://localhost:3000/register`, {
    mode: 'cors',
    method: 'POST',
    body: JSON.stringify(values),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  })
    .then(response => response.json())
    .then(data => {
      if (data.message) {
        props.enqueueSnackbar(data.message,{variant:'error'})
      } else {
        props.enqueueSnackbar('You have registered successfully',{variant:'success'})
        props.history.push('/login');
      }
})
  }
  return (
    <div className='Page'>
     <div className="Register">
      <form className='Form' onSubmit={handleSubmit(onSubmit)} style={{marginTop:'75px'}}>
      <h1 className="h1">Register</h1>
        <div>
          <label className='label' htmlFor="username">Username</label>
          <input className='input' type="text" placeholder="Username" name="username" ref={register({required: true, max: 25, min: 3, maxLength: 25})} />
          {errors.username && <p className='para'>Username should be between 3 and 25 characters</p>}
        </div>
        <div>
          <label className='label' htmlFor="first_name">First Name</label>
          <input className='input' type="text" placeholder="First Name" name="first_name" ref={register({required: true, maxLength: 80})} />
          {errors.first_name && <p className='para'>This is required</p>}
        </div>
        <div>
          <label className='label' htmlFor="last_name">Last Name</label>
          <input className='input' type="text" placeholder="Last name" name="last_name" ref={register({required: true, maxLength: 100})} />
          {errors.last_name && <p className='para'>This is required</p>}
        </div>
        <div>
          <label className='label' htmlFor="password">Password</label>
          <input className='input' type="password" placeholder="Password" name="password" ref={register({required: true, max: 50, min: 8, maxLength: 50, pattern: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/i})} />
          {errors.password && <p className='para'>Password should be in the range 8-50 and contain atleast 1 uppercase letter,1 lowercase letter,1 special character,1 number</p>}
        </div>
       
          <input className='input' type="submit" />
      </form>
     </div>
    </div>
  );
  }

  export default withSnackbar(Register);
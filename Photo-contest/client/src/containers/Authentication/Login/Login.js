import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../../Context/AuthContext';
import { setUserSession } from '../../../Utils/Common';
import { withSnackbar } from 'notistack';

const Signin = (props) => {

  const { setLoginState } = useContext(AuthContext);
  const [isFormValid, setIsFormValid] = useState(false);

  const [form, setForm] = useState({
    username: {
      placeholder: 'username',
      value: '',
      validations: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    password: {
      placeholder: 'password',
      value: '',
      validations: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    const data = Object.keys(form).reduce((acc, elementKey) => {
      return {
        ...acc,
        [elementKey]: form[elementKey].value,
      };
    }, {});
    fetch(`http://localhost:3000/auth/login`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message) {
          props.enqueueSnackbar(data.message, { variant: 'error' });
          return;
        } else {
          props.enqueueSnackbar('You logged in successfully', { variant: 'success' });
          setUserSession(data.token);
          props.history.push('/dashboard');
          setLoginState(true);
        }
      });
  };

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }
    return isValid;
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    const updatedElement = { ...form[name] };
    updatedElement.value = value;
    updatedElement.touched = true;
    updatedElement.valid = isInputValid(value, updatedElement.validations);

    const updatedForm = { ...form, [name]: updatedElement };
    setForm(updatedForm);

    const formValid = Object.values(updatedForm).every((elem) => elem.valid);
    setIsFormValid(formValid);
  };

  return (
    <div className="container">
      <div className="row justify-content-center" style={{ marginTop: '75px' }}>
        <div className="col-4">
          <h1 className="login-title text-center">Sign in</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <input
                type="text"
                name="username"
                id="username"
                className="form-control"
                placeholder={form.username.placeholder}
                value={form.username.value}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group mb-4">
              <input
                type="password"
                name="password"
                id="password"
                className="form-control"
                placeholder={form.password.placeholder}
                value={form.password.value}
                onChange={handleInputChange}
              />
            </div>
            <button
              name="login"
              id="login"
              className="btn btn-block btn-outline-primary"
              type="submit"
              disabled={!isFormValid}
            >
              Signin
            </button>
          </form>
          <p className="login-wrapper-footer-text">
            Don't have an account?
            <Link to="/register" className="text-reset">
              Register here
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default withSnackbar(Signin);

import React, { useState, useEffect, Fragment } from 'react';
import httpProvider from '../providers/http-provider';
import { withRouter } from 'react-router-dom';
import CircularProgress from '../coponents/Loader/Loader';
import { getUser } from '../Utils/Common';
import UploadPhoto from '../coponents/Photos/UploadPhotoPage';
import ViewPhotos from '../coponents/Photos/ViewPhotosPage';

const IndividualContest = (props) => {
  const user = getUser();
  const contestId = props.match.params.id;
  const [loading, setLoading] = useState(false);
  const [isjury, setIsJury] = useState(true);
  const [contest, setContest] = useState([]);
  const [photos, setPhotos] = useState([]);
  const [uploaded, setUploaded] = useState([]);
  const [phasetime, setPhasetime] = useState([]);
  const [allPhotosError, setallPhotosError] = useState(null);
  const [joinedContests, setJoinedContests] = useState(false);
  const [uploadedPhoto, setUploadedPhoto] = useState([]);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/contests/${contestId}`)
      .then((data) => setIsJury(data.jury.some((jury) => jury.user_id === user.sub)))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/contests/${contestId}`)
      .then((data) => setContest(data))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/photos/${contestId}/all`)
      .then((data) => setPhotos(data))
      .catch((error) => setallPhotosError(error.message))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/contests/contest/${contestId}/joined`)
      .then((data) => setJoinedContests(data))
      .catch((error) => error.message)
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/photos/${contestId}/picture`)
      .then((data) => {
        if (!data.message) {
          setUploaded(data);
        } else {
          setUploadedPhoto(data.message);
        }
      })
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    httpProvider
      .get(`http://localhost:3000/contests/${contestId}/timelimit`)
      .then((data) => setPhasetime(data));
  }, [contestId]);

  if (loading) {
    return <CircularProgress />;
  }

  return (
    <Fragment>
      {contest.length !== 0 && (
        <>
          {contest.contest.status === 'Phase1' && (
            <h2 style={{ marginTop: '100px',textAlign:'center'}}>
              Days: {phasetime.daysUntilPhase2} Hours: {phasetime.timeUntilPhase2} Minutes:{' '}
              {phasetime.minutesUntilPhase2} Until Phase 2
            </h2>
          )}
          {contest.contest.status === 'Phase2' && (
            <h1 style={{ marginTop: '100px',textAlign:'center'}}>
              Days: {phasetime.daysUntilFinished} Hours: {phasetime.timeUntilFinished} Minutes:{' '}
              {phasetime.minutesUntilFinished} Until Contest finishes
            </h1>
          )}
        </>
      )}
      {isjury && contest.length !== 0 && !allPhotosError ? (
        <ViewPhotos photos={photos} contest={contest} />
      ) : (
          <h1>{allPhotosError}</h1>
        )}
      {
        contest?.contest?.status === 'Phase1' && !isjury ?
          (joinedContests?.length === 0 ?
            <h1>You need to join contest in order to upload photo</h1>
            :
            (uploaded?.length === 0 ?
              <UploadPhoto />
              :
              <h1>You have already uploaded a photo</h1>
            )
          )
          :
          null
      }
      {contest?.contest?.status === 'Phase2' && !isjury ?
        (joinedContests?.length === 0 ?
          <h1>Conest is no longer accepting enrolls</h1>
          :
          (uploaded?.length === 0 ?
            <h1>You cannot upload photo for this contest anymore</h1>
            :
            <h1>Contest result will appear after Phase2</h1>
          )
        )
        :
        null
      }
      {contest?.contest?.status === 'Finished' && !isjury ?
        (joinedContests?.length === 0 ?
          <h1>Contest is no longer accepting enrolls</h1>
          :
          <ViewPhotos photos={photos} contest={contest} photo={uploaded} />
        )
        :
        null
      }
    </Fragment>
  );
};

export default withRouter(IndividualContest);

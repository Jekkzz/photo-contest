import React, { useState,useEffect } from 'react';
import { getUser } from '../Utils/Common';
import httpProvider from '../providers/http-provider';
import UserRank from '../coponents/Dashboard/UserRank';
import Users from '../coponents/Dashboard/Users'
import CircularProgress from '../coponents/Loader/Loader';

const Dashboard = () => {
  
  const user = getUser();
  const [users,setUsers] = useState([]);
  const [userRank,setUserRank] = useState([]);
  const [loading, setLoading] = useState(false);

    useEffect(()=>{
        setLoading(true)
        httpProvider.get(`http://localhost:3000/register`)
        .then((data) => setUsers(data))
        .finally(() => setLoading(false));
    },[])

    useEffect(()=>{
        setLoading(true)
        httpProvider.get(`http://localhost:3000/register/rank`)
        .then((data) => setUserRank(data))
        .finally(() => setLoading(false));
    },[])
    
    if (loading) {
        return <CircularProgress/>
    }

  return (
    <div>
      {user.role !== 'Organizer' ?
            <div className="Container">
            <UserRank data={userRank}
            />
             </div>
          
    :
            <div className="UsersContainer">
            <Users data={users}
            />
             </div>
    }
    </div>
  );
};

export default Dashboard;

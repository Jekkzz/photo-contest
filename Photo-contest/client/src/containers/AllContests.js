import React, { useState, useEffect, Fragment } from 'react';
import httpProvider from '../providers/http-provider';
import CircularProgress from '../coponents/Loader/Loader';
import Contests from '../coponents/Contests/Contests';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(10),
    paddingBottom: theme.spacing(10),
    display: 'flex',
  },
}));

const AllContests = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [phase1, setPhase1] = useState([]);
  const [phase2, setPhase2] = useState([]);
  const [finished, setFinished] = useState([]);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/contests?status='Phase1'`)
      .then((data) => setPhase1(data))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/contests?status='Phase2'`)
      .then((data) => setPhase2(data))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`http://localhost:3000/contests?status='Finished'`)
      .then((data) => setFinished(data))
      .finally(() => setLoading(false));
  }, []);


  if (loading) {
    return <CircularProgress />;
  }

  const transformedContests =
    phase1.length &&
    phase1.map((contest) => {
      return <Contests key={contest.contest_id} {...contest} />;
    });

  const transformedContests2 =
    phase2.length &&
    phase2.map((contest) => {
      return <Contests key={contest.contest_id} {...contest} />;
    });

  const transformedContests3 =
    finished.length &&
    finished.map((contest) => {
      return <Contests key={contest.contest_id} {...contest} />;
    });

  return (
    <Fragment>
      <Container style={{ marginTop: '70px' }}>
        <Typography variant="h1">Phase 1 contests</Typography>
        <Grid container spacing={8} className={classes.cardGrid} maxwidth="md">
          {phase1.length ? <>{transformedContests}</> : <div style={{display:'contents'}}><h2 style={{margin:'auto'}}>No contests to show ...</h2></div>}
        </Grid>
      </Container>
      <Container style={{ marginTop: '70px' }}>
        <Typography variant="h1">Phase 2 contests</Typography>
        <Grid container spacing={8} className={classes.cardGrid} maxwidth="md">
          {phase2.length ? <>{transformedContests2}</> : <div style={{display:'contents'}}><h2 style={{margin:'auto'}}>No contests to show ...</h2></div>}
        </Grid>
      </Container>
      <Container style={{ marginTop: '70px' }}>
        <Typography variant="h1">Finished</Typography>
        <Grid container spacing={8} className={classes.cardGrid} maxwidth="md">
          {finished.length ? <>{transformedContests3}</> :<div style={{display:'contents'}}><h2 style={{margin:'auto'}}>No contests to show ...</h2></div>}
        </Grid>
      </Container>
    </Fragment>
  );
};

export default AllContests;

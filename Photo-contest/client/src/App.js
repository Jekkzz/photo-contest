import './App.css';
import React, { useState } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { AuthContext } from './Context/AuthContext';
import { getToken } from './Utils/Common';
import Login from './containers/Authentication/Login/Login';
import Nav from './coponents/Nav/Nav';
import Content from './views/Index/Content';
import Contests from './coponents/Contests/Contest'
import Footer from './coponents/Footer/Footer';
import Register from './containers/Authentication/Register/Register';
import { SnackbarProvider } from 'notistack';
import Dashboard from './containers/Dashboard';
import CreateContest from './coponents/Contests/CreateContest';
import AllContests from './containers/AllContests';
import IndividualContest from './containers/IndividualContest';
import IndividualPhotoReview from './coponents/Photos/IndividualPhotoReview';
import IndividualPhotoPage from './coponents/Photos/IndividualPhotoPage';

function App() {
  const token = getToken();
  const [authValue, setAuthValue] = useState(token ? true : false);
  
  return (
    <SnackbarProvider maxSnack={3} anchorOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}>
      <BrowserRouter>
        <AuthContext.Provider value={{ isLoggedIn: authValue, setLoginState: setAuthValue }}>
          <Nav />
          <div className="main-container">
            <Switch>
              {!token
                ?
                <>
                <Route exact path="/" component={Content} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                </>
            : 
            <>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/create" component={CreateContest} />
              <Route exact path="/dashboard" component={Dashboard} />
              <Route exact path="/contests/:type" component={Contests} />
              <Route exact path="/contests/contests/:id" component={IndividualContest}/>
              <Route exact path="/contests/contests/:id/photos/:pathParam2?/review" component={IndividualPhotoReview} />
              <Route exact path="/contests/contests/:id/photos/:pathParam2?" component={IndividualPhotoPage} />
              <Route exact path="/dashboard/contests" component={AllContests} />
              </>
            }
            </Switch>
            <Footer />
          </div>
        </AuthContext.Provider>
      </BrowserRouter>
    </SnackbarProvider>
  );
}

export default App;

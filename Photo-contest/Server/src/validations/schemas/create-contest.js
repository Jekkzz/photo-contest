export const createContestSchema = {
    title: value => {
        if (!value) {
            return 'Title is required!';
        }
        if (typeof value !== 'string' || value.length < 3 || value.length > 45) {
            return 'Title should be a string in range [3...45]';
        }
        return null;
    },
};
export const createUserSchema = {
    username: (value) => {
      if (!value) {
        return 'Username is required';
      }
  
      if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
        return 'Username should be a string in range [3..25]';
      }
  
      return null;
    },
    password: (value) => {
      if (!value) {
        return 'Password is required';
      }
      const regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
      if (!regex.test(value)) {
        return 'Password should contain atleast 1 uppercase letter,1 lowercase letter,1 special character,1 number';
      }
      if (typeof value !== 'string' || value.length < 8 || value.length > 50) {
        return 'Password should be a string in range [8..50]';
      }
  
      return null;
    },
    first_name: (value) => {
      if (!value) {
        return 'First Name is required';
      }
      return null;
    },
    last_name: (value) => {
      if (!value) {
        return 'Last Name is required';
      }
      return null;
    },
  };
  
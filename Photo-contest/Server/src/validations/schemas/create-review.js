export const createReviewSchema = {
    score: value => {
        if (!value) {
            return 'Score is required!';
        }
        if (value < 1 || value > 10) {
            return 'Review score should be a number in range [1...10]';
        }
        return null;
    },
    comment: value => {
        if (!value) {
            return 'Comment is required!';
        }
        if (typeof value !== 'string' || value.length < 5 || value.length > 250) {
            return 'Review comment should be a number in range [5...250]';
        }
        return null;
    },
    wrongCategory: value => {
        if (typeof value !== 'boolean') {
            return 'Not fit to category should be a boolean';
        }
        return null;
    },
};

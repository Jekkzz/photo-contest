import express from 'express';
import createToken from '../auth/create-token.js';
import usersService from '../services/users-service.js';
import usersData from '../data/users-data.js';
import serviceErrors from '../services/service-errors.js';

const authController = express.Router();

authController
  .post('/login', async (req, res) => {
    const { username, password } = req.body;
    const { error, user } = await usersService.signInUser(usersData)(username, password);

    if (error === serviceErrors.INVALID_SIGNIN) {
      res.status(400).send({
        message: 'Invalid username/password',
      });
    }

    if (error) {
      res.status(403).send({
        message: error,
      });
    } else {
      const payload = {
        sub: user.user_id,
        username: user.username,
        role: user.role,
        first_name: user.first_name,
        last_name: user.last_name,
        points: user.points,
      };

      const token = createToken(payload);
      
      res.status(200).send({
        token: token,
      });
    }
  });

export default authController;

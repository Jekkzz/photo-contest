import express from 'express';
import usersService from '../services/users-service.js';
import usersData from '../data/users-data.js';
import serviceErrors from '../services/service-errors.js';
import { createValidator, createUserSchema } from '../validations/index.js';
import { authMiddleware } from '../auth/auth-middleware.js';

const usersController = express.Router();

usersController
  .post('/', createValidator(createUserSchema), async (req, res) => {
    const createData = req.body;
    const { error, user } = await usersService.createUser(usersData)(createData);

    if (error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message: 'Username not available' });
    } else {
      res.status(201).send(user);
    }
  })
  .get('/', async (req, res) => {
    const { error, users } = await usersService.getAllPhotoJunkiesOrderedByRanks(usersData)();

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'No Photo Junkies exist' });
    } else {
      res.status(200).send(users);
    }
  })
  .get('/rank',authMiddleware, async (req, res) => {
    const user_id = req.user.id;
    const { error, points,userrank,username,treshhold,nextrank } = await usersService.getCurrentPointsAndRank(usersData)(user_id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'No such user exist' });
    } else {
      res.status(200).send({points,treshhold,nextrank,userrank,username});
    }
  })
  .get('/jury', async (req, res) => {
    const { error, users } = await usersService.getAllUsersWithMasterAndDictatorRank(usersData)();
    
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'No Users with rank Master/Dictator yet' });
    } else {
      res.status(200).send(users);
    }
  });

export default usersController;

import express from 'express';
import contestServices from '../services/contest-service.js';
import serviceErrors from '../services/service-errors.js';
import contestData from '../data/contest-data.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import { createContestSchema, createValidator } from '../validations/index.js';
import { rankUpMiddleware } from '../middleware/rankup-middleware.js';
import photosData from '../data/photos-data.js';
const contestController = express.Router();

contestController
  //Get all contests
  .get('/', authMiddleware, rankUpMiddleware, async (req, res) => {
    const { status } = req.query;
    const { error, contests } = await contestServices.getAllContests(contestData)(status);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'Contest not found' });
    } else {
      res.status(200).send(contests);
    }
  })
  //get contest by id
  .get('/:id', authMiddleware, rankUpMiddleware, async (req, res) => {
    const { id } = req.params;
    const { error, contest, jury } = await contestServices.getContestById(contestData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'Contest not found' });
    } else {
      res.status(200).send({ contest, jury });
    }
  })
  //Get joined contests by user id
  .get('/contests/user', authMiddleware, async (req, res) => {
    const user_id = req.user.id;
    const { error, contests } = await contestServices.getActiveJoinedContests(contestData)(+user_id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No joined contests' });
    } else {
      res.status(200).send(contests);
    }
  })
  .get('/contest/:id/joined', authMiddleware, async (req, res) => {
    const user_id = req.user.id;
    const { id } = req.params;
    const { error, joinedContest } = await contestServices.getJoinedContestByIdandUserId(contestData)(+id,+user_id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No joined contests' });
    } else {
      res.status(200).send(joinedContest);
    }
  })
  //Get contests phase time
  .get('/:id/timelimit', authMiddleware, async (req, res) => {
    const { id } = req.params;
    const { error, time } = await contestServices.getPhaseTimeLimit(contestData,photosData)(+id);

    if (error === serviceErrors.NOT_FOUND) {
      res.status(404).send({ message: 'Contest not found' });
    } else {
      res.status(200).send(time);
    }
  })
  //Award points at end of contest
  .get('/:id/points', authMiddleware, async (req, res) => {
    const { id } = req.params;
    const { error, points } = await contestServices.getAveragePointsByContest(contestData)(+id);

    if (error === serviceErrors.NOT_FOUND) {
      res.status(409).send({ message: `There is no contest with id:${id}` });
    }

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res.status(409).send({ message: 'Contest has not finished!' });
    }

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(409).send({ message: 'There are to photos rated for this contest' });
    }

    if (error === serviceErrors.OPERATION_ALREADY_COMPLETED) {
      res.status(409).send(points);
    } else {
      res.status(200).send(points);
    }
  })
  //Get finished contests by user id
  .get('/contests/finished', authMiddleware, async (req, res) => {
    const user_id = req.user.id;
    const { error, contests } = await contestServices.getFinishedContestsById(contestData)(
      +user_id,
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No finished contests yet' });
    } else {
      res.status(200).send(contests);
    }
  })
  //Get all open Contests
  .get('/contests/open', authMiddleware, async (req, res) => {
    const user_id = req.user.id;
    const { error, contests } = await contestServices.getOpenContestsInPhase1(contestData)(+user_id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No contests in Phase 1 exist at this time' });
    } else {
      res.status(200).send(contests);
    }
  })
  //Join Contest
  .post('/:id', authMiddleware, async (req, res) => {
    const { id } = req.params;
    const user_id = req.user.id;
    const { error, contest } = await contestServices.joinContestByUserId(contestData)(user_id, +id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'Contest not found' });
    }
    if (error === serviceErrors.OPERATION_ALREADY_COMPLETED) {
      res.status(409).send({ message: 'You have already joined the contest' });
    } if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res.status(409).send({ message: 'Jury can not join the contest' });
    } else {
      res.status(200).send(contest);
    }
  })
  //Create Contest
  .post('/', authMiddleware, createValidator(createContestSchema), async (req, res) => {
    const contest = {
      title: req.body.title,
      phase1: req.body.phase1,
      phase2: req.body.phase2,
      category: req.body.category,
      type: req.body.type,
      jury: req.body.jury,
      invite: req.body.invite,
    };

    const { error, newContest } = await contestServices.createContest(contestData)(contest);
    
    if (error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message: 'Contest title already exists!' });
    } else {
      res.status(201).send(newContest);
    }
  });

export default contestController;

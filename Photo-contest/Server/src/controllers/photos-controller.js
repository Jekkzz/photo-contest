import express from 'express';
import photosService from '../services/photos-service.js';
import photosData from '../data/photos-data.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import storage from '../storage.js';
import multer from 'multer';
import { createValidator, submitPhotoSchema, createReviewSchema } from '../validations/index.js';
import serviceErrors from '../services/service-errors.js';
const photossController = express.Router();

photossController
  .post(
    '/photos/:id/picture',
    authMiddleware,
    createValidator(submitPhotoSchema),
    multer({ storage: storage }).single('avatar'),
    async (req, res) => {
      const { id } = req.params;
      const { photo_story, photo_title } = req.body;
      const { error } = await photosService.submitPhoto(photosData)(req.user.id, id, photo_story, photo_title, req.file.filename);

      if (error) {
        res.sendStatus(500);
      } else {
        res.status(200).send({
          path: req.file.filename,
        });
      }
    },
  )
  .get('/photos/:id/picture',
    authMiddleware,
    async (req, res) => {
      const { id } = req.params;
      const { error,photo } = await photosService.getUploadedPhoto(photosData)(req.user.id, id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(409).send({ message: 'No photo uploaded' });
      } else {
        res.status(200).send(photo);
      }
    },
  )
  .get('/photos/:id/review',
    authMiddleware,
    async (req, res) => {
      const { id } = req.params;
      const user_id = req.user.id;
      const { error, review } = await photosService.getReviewByJuryId(photosData)(+id, user_id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Review does not exist!' });
      } else {
        res.status(200).send(review);
      }
    },
  )
  .get('/photos/:id',
    authMiddleware,
    async (req, res) => {
      const user_id = req.user.id;
      const { id } = req.params;
      const { error, photos } = await photosService.viewPhotosAndReviewsOfOtheUsers(photosData)(+user_id, +id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'No photos are rated by other users yet!' });
      } else {
        res.status(200).send(photos);
      }
    },
  )
  .get('/photos/:id/all',
    authMiddleware,
    async (req, res) => {
      const { id } = req.params;
      const user_id = req.user.id;
      const { error, photos } = await photosService.getAllPhotosByContestId(photosData)(+id,+user_id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'No photos for this contest!' });
      } else {
        res.status(200).send(photos);
      }
    },
  )
  .get('/photos/:id/reviews',
    authMiddleware,
    async (req, res) => {
      const { id } = req.params;
      const { error, photoAndReviews } = await photosService.viewPhotosAndReviewsOfOtheUsers(photosData)(+id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'No photos are rated by other users yet!' });
      } else {
        res.status(200).send(photoAndReviews);
      }
    },
  )
  .post('/contests/:id/photos/:photoId/reviews',
    authMiddleware,
    createValidator(createReviewSchema),
    async (req, res) => {
      const { id, photoId } = req.params;
      const user_id = req.user.id;

      const review = {
        photo_id: +photoId,
        score: req.body.score,
        comment: req.body.comment,
        wrongCategory: req.body.wrongCategory,
        user_id: +user_id,
      };

      const { error, createReview } = await photosService.createReview(photosData)(review, +id);

      if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(404).send({ message: 'Only one review per jury can be created!' });
      } 
      if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
        res.status(404).send({ message: 'Only jury can give a review!' });
      } else {
        res.status(201).send(review);
      }
    });

export default photossController;

/* eslint-disable no-unused-vars */
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import { PORT } from './config.js';
import passport from 'passport';
import contestController from './controllers/contest-controller.js';
import jwtStrategy from './auth/strategy.js';
import authController from './controllers/auth-controller.js';
import usersController from './controllers/users-controller.js';
import photosController from './controllers/photos-controller.js';

const app = express();
passport.use(jwtStrategy);
app.use(cors(), bodyParser.json(), helmet());
app.use(passport.initialize());
app.use('/contests', contestController);
app.use('/auth', authController);
app.use('/register', usersController);
app.use('/', photosController);
app.use('/public', express.static('images'));
app.use((err, req, res, next) => {
  res.status(500).send({
    message: 'An unexpected error occurred, our developers are working hard to resolve it.',
  });
});
app.all('*', (req, res) => res.status(404).send({ message: 'Resource not found!' }));

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));

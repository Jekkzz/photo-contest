import pool from './pool.js';

const submitPhoto = async (id, contest_id, photo_story, photo_title, path) => {
  const sql = `
        INSERT INTO photos(user_id,contest_id,photo_story,photo_title,photo_path)
        VALUES(?,?,?,?,?)
    `;

  return await pool.query(sql, [id, contest_id, photo_story, photo_title, path]);
};

const getUploadedPhoto = async (id, contest_id) => {
  const sql = `
  SELECT p.photo_story,p.photo_path,p.photo_title,u.username,p.photo_id FROM
  photos as p
  RIGHT JOIN users as u
  on p.user_id = u.user_id
      WHERE p.user_id = ? && contest_id = ?
    `;
  const result = await pool.query(sql, [id, contest_id]);
  return result?.[0];
};
const createPhotoReview = async ({ photo_id, score, comment, wrongCategory, user_id }) => {
  const sql = `
    INSERT INTO photos_reviews (photo_id, photo_score, photo_comment, not_fit_to_category, user_id)
    VALUES (?, ?, ?, ?, ?)
    `;
  const result = await pool.query(sql, [photo_id, score, comment, wrongCategory, user_id]);
  const sql2 = `
    SELECT p.review_id, p.photo_score, p.photo_comment, u.username as jury
    FROM photos_reviews AS p
    JOIN users AS u ON p.user_id = u.user_id
    WHERE review_id = ?
    `;
  const createdReview = await pool.query(sql2, [result.insertId]);

  return createdReview?.[0];
};

const getJuryById = async (user_id, contest_id) => {
  const sql = `
    SELECT * FROM jury
    WHERE user_id = ? && contest_id = ?
    `;
  const result = await pool.query(sql, [user_id, contest_id]);
  return result?.[0];
};

const getJuryReview = async (photo_id, user_id) => {
  const sql = `
    SELECT * FROM photos_reviews 
    WHERE user_id = ? && photo_id= ?
    `;

  const result = await pool.query(sql, [user_id, photo_id]);
  return result?.[0];
};
const viewPhotoandReviewsByPhotoId = async (photo_id) => {
  const sql = `
  SELECT u.username,u.first_name,u.last_name,p.photo_story,p.photo_title,p.photo_path,pr.photo_score,pr.photo_comment,us.username as jury
  FROM photos AS p
  RIGHT JOIN photos_reviews AS pr
  ON p.photo_id = pr.photo_id
  LEFT JOIN users AS u
  ON p.user_id = u.user_id
  LEFT JOIN contests AS c
  ON p.contest_id = c.contest_id
  LEFT JOIN users as us
  on pr.user_id = us.user_id
  WHERE p.photo_id = ? ;
    `;
  const result = await pool.query(sql, [photo_id]);
  return result;
};
const getAllPhotos = async (contest_id,user_id) => {
  const sql = `
  SELECT p.photo_id,p.photo_story,p.photo_title,p.photo_path,u.username,p.place,p.contest_id
  from photos as p
  LEFT JOIN users as u
  ON p.user_id = u.user_id
  where p.contest_id = ? && p.user_id !=?
  `;
  const result = await pool.query(sql, [contest_id,user_id]);
  return result;
};
export default {
  submitPhoto,
  getUploadedPhoto,
  createPhotoReview,
  getJuryReview,
  getJuryById,
  viewPhotoandReviewsByPhotoId,
  getAllPhotos,
};

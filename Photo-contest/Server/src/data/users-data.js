import pool from './pool.js';

const getAll = async () => {
  const sql = `
  SELECT u.id, u.username, u.password, r.role as role,u.first_name,u.last_name
  FROM users u
  JOIN roles r ON u.role_id = r.role_id
    `;

  return await pool.query(sql);
};

const getWithRole = async (username) => {
  const sql = `
       SELECT u.user_id, u.username, u.password, r.role as role,u.first_name,u.last_name,u.points
       FROM users u
       JOIN roles r ON u.role_id = r.role_id
       WHERE u.username = ?
    `;

  const result = await pool.query(sql, [username]);

  return result[0];
};
const getAllPhotoJunkiesOrderedByRank = async () => {
  const sql = `
  SELECT u.user_id,u.username,ur.user_rank,u.first_name,u.last_name,r.role
  FROM users u 
  JOIN roles r ON u.role_id = r.role_id
  RIGHT JOIN user_rank ur
  ON u.user_rank_id = ur.id
  WHERE r.role = 'Photo Junkie'
  ORDER BY FIELD(ur.user_rank,'Wise and Benevolent Photo Dictator','Master','Enthusiast','Junkie')
  `;
  const result = await pool.query(sql);
  return result;
};
const getBy = async (column, value) => {
  const sql = `
        SELECT u.user_id, u.username,r.role as role,u.first_name,u.last_name
        FROM users u
        JOIN roles r ON u.role_id = r.role_id
        WHERE u.${column} = ?
    `;

  const result = await pool.query(sql, [value]);

  return result[0];
};

const create = async (username, password, role,rank, first_name, last_name) => {
  const sql = `
        INSERT INTO users(username, password, role_id,user_rank_id,first_name,last_name)
        VALUES (?,?,(SELECT role_id FROM roles WHERE role = ?),(SELECT id FROM user_rank WHERE user_rank = ?),?,?)
    `;

  const result = await pool.query(sql, [username, password, role,rank, first_name, last_name]);

  return {
    id: result.insertId,
    username: username,
  };
};
const getUsersWithMasterAndDictatorRank = async () => {
  const sql = `
  SELECT ur.user_rank,u.user_id,u.username
  FROM users u
  JOIN user_rank ur 
  ON u.user_rank_id = ur.id
  WHERE ur.user_rank = 'Master' || ur.user_rank = 'Wise and Benevolent Photo Dictator' 
  `;
  const result = await pool.query(sql);
  return result;
};
const rankUpUser = async (rank, user_id) => {
  const sql = `
  UPDATE users 
  SET user_rank_id=(select id from user_rank where user_rank.user_rank=?)
  WHERE users.user_id = ?
  `;
  const result = await pool.query(sql, [rank, user_id]);
  return result;
};
const getUserPoints = async (user_id) => {
  const sql = ` 
  SELECT points FROM users WHERE user_id = ?
  `;
  const result = await pool.query(sql, [user_id]);
  return result[0];
};
const getUserPointsAndRank = async (user_id) => {
  const sql = `
  SELECT u.points,ur.user_rank,u.username
  FROM users u
  RIGHT JOIN user_rank ur   
  ON u.user_rank_id = ur.id
  WHERE u.user_id = ?
  `;
  const result = await pool.query(sql, [user_id]);
  return result[0];
};
export default {
  getAll,
  getBy,
  create,
  getWithRole,
  getAllPhotoJunkiesOrderedByRank,
  getUsersWithMasterAndDictatorRank,
  rankUpUser,
  getUserPoints,
  getUserPointsAndRank,
};

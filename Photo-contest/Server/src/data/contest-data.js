import pool from './pool.js';

const getAll = async () => {
  const sql = `
SELECT c.contest_id, c.title, c.phase1_time_limit, c.phase2_time_limit, c.category, t.type
FROM contests AS c
LEFT JOIN type AS t 
ON t.id = c.type_id
`;

  return await pool.query(sql);
};

const searchBy = async (column, value) => {
  const sql = ` 
  SELECT * FROM contests as c
  WHERE c.${column} = ${value}
  `;
  return await pool.query(sql);
};

const getCurrentContestsJoined = async (user_id) => {
  const sql = `
  SELECT c.title, c.phase1_time_limit, c.phase2_time_limit, c.category,c.contest_id, uc.user_id, j.user_id as jury
  FROM contests AS c
  RIGHT JOIN users_contests AS uc
  ON uc.contest_id = c.contest_id
  LEFT JOIN jury as j
  ON j.contest_id = c.contest_id
  WHERE uc.user_id = ? && c.status != 'Finished'
    `;

  const contests = await pool.query(sql, [user_id]);
  const map = new Map();

  for (const contest of contests) {
    const {
      contest_id,
      title,
      phase1_time_limit,
      phase2_time_limit,
      category,
      type_id,
      user_id,
      jury,
    } = contest;
    if (!map.has(contest_id)) {
      map.set(contest_id, {
        contest_id,
        title,
        phase1_time_limit,
        phase2_time_limit,
        category,
        type_id,
        user_id,
        jury: [],
      });
    }

    map.get(contest_id).jury.push(jury);
  }
  return [...map.values()];
};
const getJoinedContestByUserIdAndContestId = async (contest_id, user_id) => {
  const sql = `
    SELECT *
    FROM users_contests
    WHERE contest_id = ? && user_id = ?
  `;
  const result = await pool.query(sql, [contest_id, user_id]);
  return result;
};
const getBy = async (column, value) => {
  const sql = `
    SELECT c.contest_id, c.title, c.phase1_time_limit, c.phase2_time_limit, c.category, t.type,c.status
    FROM contests AS c
    LEFT JOIN type AS t 
    ON t.id = c.type_id
    WHERE c.${column} = ?
    `;

  const result = await pool.query(sql, [value]);
  return result[0];
};

const getJuryAndContestById = async (contest_id, user_id) => {
  const sql = `
    SELECT * FROM JURY
    where contest_id = ? && user_id = ?
  `;
  const result = await pool.query(sql, [contest_id, user_id]);
  return result?.[0];
};

const getJuryById = async (contest_id) => {
  const sql = `
    SELECT user_id FROM JURY
    where contest_id = ?
  `;
  const result = await pool.query(sql, [contest_id]);
  return result;
};

const joinContest = async (user_id, contest_id) => {
  const sql = `
    INSERT INTO users_contests(user_id,contest_id)
    VALUES(?,?)
    `;

  const result = await pool.query(sql, [user_id, contest_id]);
  return result?.[0];
};
const getNoReviewedPhotos = async (contest_id) => {
  const sql = `
    SELECT * FROM
    photos as p
    RIGHT JOIN photos_reviews as pr
    ON pr.photo_id = p.photo_id
    where p.contest_id = ? && pr.photo_score = 0 && pr.isReviewed = 0
  `;
  const result = await pool.query(sql, [contest_id]);
  return result;
};
const getReviewdPhotoByJuryIdAndPhotoId = async (user_id, photo_id) => {
  const sql = `SELECT pr.photo_id,pr.photo_score,pr.user_id from photos_reviews as pr
  RIGHT JOIN photos as p
  ON pr.photo_id = p.photo_id
  where pr.user_id = ? && pr.photo_id = ?
  `;
  const result = await pool.query(sql, [user_id, photo_id]);
  return result[0];
};
const createContest = async ({ title, phase1, phase2, category, type }) => {
  const sql = `
    INSERT INTO contests (title, phase1_time_limit, phase2_time_limit, category, type_id)
    VALUES (?, DATE_ADD(NOW(), INTERVAL ? DAY),DATE_ADD(DATE_ADD(NOW(), INTERVAL ? DAY), INTERVAL ? HOUR), ?, (SELECT id FROM type WHERE type = ?));
    `;
  const result = await pool.query(sql, [title, phase1, phase1, phase2, category, type]);

  const selectUserId = `
  SELECT u.user_id from users as u
  LEFT JOIN roles as r
  ON u.role_id = r.role_id
  WHERE r.role = 'Organizer'
  `;

  const selectConstestId = `
  SELECT contest_id FROM contests WHERE contest_id = ?;
  `;

  const selectUserIdResult = await pool.query(selectUserId);
  const selectConstestIdRes = await pool.query(selectConstestId, [result.insertId]);
  let values = '';
  selectUserIdResult.map((val, index) => {
    if (selectUserIdResult.length - 1 === index) {
      values += `(${val.user_id}, ${selectConstestIdRes[0].contest_id})`;
    } else {
      values += `(${val.user_id}, ${selectConstestIdRes[0].contest_id}) ,`;
    }
  });

  const insertJury = `
  INSERT INTO jury (user_id, contest_id)
  VALUES ${values}
  `;
  await pool.query(insertJury);

  const selectContestById = `
    SELECT * FROM contests 
    WHERE contest_id = ?
    `;

  const createdContest = await pool.query(selectContestById, [result.insertId]);
  return createdContest?.[0];
};

const selectJury = async (user_id, contest_id) => {
  const sql = `
    INSERT INTO jury (user_id,contest_id)
    VALUES(?,?)
  `;
  return await pool.query(sql, [user_id, contest_id]);
};

const getPhasesTimeLimit = async (id) => {
  const sql = `
    SELECT phase1_time_limit,phase2_time_limit
    FROM contests
    WHERE contest_id = ?
  `;

  const result = await pool.query(sql, [id]);
  return result[0];
};

const updatePhaseStatus = async (status, id) => {
  const sql = `
    UPDATE contests
    SET status = ?
    WHERE contest_id = ?
  `;

  return await pool.query(sql, [status, id]);
};

const viewFinishedContestsById = async (user_id) => {
  const sql = `
  SELECT c.title,c.contest_id, c.category, uc.user_id, c.status, j.user_id as jury
  FROM contests AS c
  RIGHT JOIN users_contests AS uc
  ON uc.contest_id = c.contest_id
  LEFT JOIN jury AS j
  ON j.contest_id = c.contest_id
  WHERE uc.user_id = ? && c.status = 'Finished'
  `;

  const contests = await pool.query(sql, [user_id]);
  const map = new Map();

  for (const contest of contests) {
    const { contest_id, title, category, user_id, status, jury } = contest;
    if (!map.has(contest_id)) {
      map.set(contest_id, {
        contest_id,
        title,
        category,
        user_id,
        status,
        jury: [],
      });
    }

    map.get(contest_id).jury.push(jury);
  }
  return [...map.values()];
};

const getOpenContestsinPhase1 = async (user_id) => {
  const sql = `
  SELECT c.contest_id,c.title,c.phase1_time_limit,c.phase2_time_limit,c.status,c.category,t.type,j.user_id as jury FROM
  contests as c
  LEFT JOIN type as t
  ON t.id = c.type_id
   LEFT JOIN jury AS j
    ON j.contest_id= c.contest_id
  where c.status = 'Phase1' && t.type ='Open' && not exists(sELECT * FROM users_contests where users_contests.contest_id = c.contest_id and users_contests.user_id = ?) || j.user_id = ?
  `;

  const contests = await pool.query(sql, [user_id, user_id,user_id]);
  const map = new Map();

  for (const contest of contests) {
    const {
      contest_id,
      title,
      phase1_time_limit,
      phase2_time_limit,
      category,
      type_id,
      user_id,
      jury,
    } = contest;
    if (!map.has(contest_id)) {
      map.set(contest_id, {
        contest_id,
        title,
        phase1_time_limit,
        phase2_time_limit,
        category,
        type_id,
        user_id,
        jury: [],
      });
    }

    map.get(contest_id).jury.push(jury);
  }
 return [...map.values()];
};

const getAveragePointsByContest = async (contest_id) => {
  const sql = `
  SELECT AVG(pr.photo_score) as average,p.user_id,u.username, p.place FROM photos AS p
  RIGHT JOIN photos_reviews as pr
  ON p.photo_id = pr.photo_id
  LEFT JOIN users as u
  ON p.user_id = u.user_id
  WHERE contest_id = ?
  group by p.user_id
  `;
  const result = await pool.query(sql, [contest_id]);
  return result;
};

const updateUserPlacing = async (placing, user_id) => {
  const sql = `
    UPDATE photos 
    SET place =?
    WHERE user_id = ?
  `;
  const result = await pool.query(sql, [placing, user_id]);
  return result;
};

const addPointsToUser = async (points, user_id) => {
  const sql = `
  UPDATE users 
  SET points = points + ? 
  WHERE user_id = ?
  `;
  const result = await pool.query(sql, [points, user_id]);
  return result;
};
export default {
  getAll,
  getBy,
  createContest,
  joinContest,
  getCurrentContestsJoined,
  getPhasesTimeLimit,
  updatePhaseStatus,
  viewFinishedContestsById,
  getOpenContestsinPhase1,
  getAveragePointsByContest,
  updateUserPlacing,
  addPointsToUser,
  selectJury,
  searchBy,
  getJuryById,
  getJuryAndContestById,
  getNoReviewedPhotos,
  getReviewdPhotoByJuryIdAndPhotoId,
  getJoinedContestByUserIdAndContestId,
};

import usersData from '../data/users-data.js';
export const rankUpMiddleware = async (req, res, next) => {
  const { id } = req.user;
  const points = await usersData.getUserPoints(id);

  if (points.points < 51) {
    next();
  }

  if (points.points > 50 && points.points < 151) {
    await usersData.rankUpUser('Enthusiast', id);
    next();
  }

  if (points.points > 150 && points.points < 1001) {
    await usersData.rankUpUser('Master', id);
    next();
  }
  
  if (points.points > 1000) {
    await usersData.rankUpUser('Wise and Benevolent Photo Dictator', id);
    next();
  }
};

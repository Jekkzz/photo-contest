/* eslint-disable no-unused-vars */
import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';
import { DEFAULT_USER_ROLE, DEFAULT_USER_RANK } from '../config.js';

const signInUser = (usersData) => {
  return async (username, password) => {
    const user = await usersData.getWithRole(username);
    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: serviceErrors.INVALID_SIGNIN,
        user: null,
      };
    }
    return {
      error: null,
      user: user,
    };
  };
};
const getAllPhotoJunkiesOrderedByRanks = (usersData) => {
  return async () => {
    const users = await usersData.getAllPhotoJunkiesOrderedByRank();
    if (!users) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        users: null,
      };
    }
    return {
      error: null,
      users: users,
    };
  };
};
const createUser = (usersData) => {
  return async (userCreate) => {
    const { username, password, first_name, last_name } = userCreate;

    const existingUser = await usersData.getBy('username', username);
    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.create(
      username,
      passwordHash,
      DEFAULT_USER_ROLE,
      DEFAULT_USER_RANK,
      first_name,
      last_name,
    );

    return { error: null, user: user };
  };
};
const getAllUsersWithMasterAndDictatorRank = (usersData) => {
  return async () => {
    const users = await usersData.getUsersWithMasterAndDictatorRank();
    if (users.length === 0) {
      return { error: serviceErrors.RECORD_NOT_FOUND, users: null };
    }
    return { error: null, users: users };
  };
};
const getCurrentPointsAndRank = (usersData) => {
  return async (user_id) => {
    const pointsAndRank = await usersData.getUserPointsAndRank(user_id);
    const currentPoints = Number(pointsAndRank.points);
    const enthusiastTreshhold = 51;
    const masterTreshhold = 151;
    const wiseAndBenevolentTreshhold = 1001;
    const points = pointsAndRank.points;
    const userRank = pointsAndRank.user_rank;
    const username = pointsAndRank.username;
    
    if (points < enthusiastTreshhold) {
      return {
        error: null,
        points: points,
        userrank: userRank,
        username: username,
        treshhold: enthusiastTreshhold - currentPoints,
        nextrank: 'Enthusiast',
      };
    }
    if (points < masterTreshhold) {
      return {
        error: null,
        points: points,
        userrank: userRank,
        username: username,
        treshhold: masterTreshhold - currentPoints,
        nextrank: 'Master',
      };
    }
    if (points < wiseAndBenevolentTreshhold) {
      return {
        error: null,
        points: points,
        userrank: userRank,
        username: username,
        treshhold: wiseAndBenevolentTreshhold - currentPoints,
        nextrank: 'Wise and Benevolent Photo Dictator',
      };
    }
    if (points >= wiseAndBenevolentTreshhold) {
      return {
        error: null,
        points: points,
        userrank: userRank,
        username: username,
        treshhold: 'You are max rank!',
      };
    }
    if (!pointsAndRank) {
      return { error: serviceErrors.RECORD_NOT_FOUND, pointsAndRank: null };
    }
    return { error: null, points: points, userrank: userRank, username: username };
  };
};
export default {
  signInUser,
  createUser,
  getAllPhotoJunkiesOrderedByRanks,
  getAllUsersWithMasterAndDictatorRank,
  getCurrentPointsAndRank,
};

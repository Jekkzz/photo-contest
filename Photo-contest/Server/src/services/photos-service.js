import serviceErrors from './service-errors.js';

const submitPhoto = (photosData) => {
  return async (id, contest_id, photo_story, photo_title, path) => {
    const result = await photosData.submitPhoto(id, contest_id, photo_story, photo_title, path);

    return { error: result.affectedRows > 0 ? null : serviceErrors.UNSPECIFIED_ERROR };
  };
};

const getUploadedPhoto = (photosData) => {
  return async (id, contest_id) => {
    const photo = await photosData.getUploadedPhoto(id, contest_id);

    if (!photo) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        photo: null,
      };
    }
    return { error: null, photo: photo };
  };
};

const getReviewByJuryId = (photosData) => {
  return async (photo_id, user_id) => {
    const review = await photosData.getJuryReview(photo_id, user_id);

    if (!review) {
      return { error: serviceErrors.RECORD_NOT_FOUND, review: null };
    }
    return { error: null, review: review };
  };
};
const createReview = (photosData) => {
  return async (review, contest_id) => {
    const getJuryReview = await photosData.getJuryReview(review.photo_id, review.user_id);
    const getJury = await photosData.getJuryById(review.user_id, contest_id);

    if (getJuryReview) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        review: null,
      };
    }
    if (!getJury) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        review: null,
      };
    }
    if (review.wrongCategory === false) {
      return await photosData.createPhotoReview(review);
    }
    const notFitToCategory = {
      ...review,
      score: 0,
      comment: 'Wrong category!',
    };

    const createReview = await photosData.createPhotoReview(notFitToCategory);
    return { error: null, createReview: createReview };
  };
};
const viewPhotosAndReviewsOfOtheUsers = (photosData) => {
  return async (photo_id) => {
    const photoAndReviews = await photosData.viewPhotoandReviewsByPhotoId(photo_id);

    if (!photoAndReviews) {
      return { error: serviceErrors.RECORD_NOT_FOUND, photos: null };
    }
    return { error: null, photoAndReviews: photoAndReviews };
  };
};
const getAllPhotosByContestId = (photosData) => {
  return async (contest_id,user_id) => {
    const photos = await photosData.getAllPhotos(contest_id,user_id);
    
    if (photos.length === 0) {
      return { error: serviceErrors.RECORD_NOT_FOUND, photos: null };
    }
    return { error: null, photos: photos };
  };
};
export default {
  submitPhoto,
  getUploadedPhoto,
  createReview,
  viewPhotosAndReviewsOfOtheUsers,
  getAllPhotosByContestId,
  getReviewByJuryId,
};

/* eslint-disable no-unused-vars */
import serviceErrors from './service-errors.js';
import moment from 'moment';
import { awardContestPoints } from '../points/points.js';

const getAllContests = (contestData) => {
  return async (filter) => {
    if (filter) {
      const contests = await contestData.searchBy('status', filter);

      if (contests.length === 0) {
        return { error: serviceErrors.RECORD_NOT_FOUND, contests: null };
      } else {
        return { error: null, contests: contests };
      }
    }

    const contests = await contestData.getAll();

    if (!contests) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contests: null,
      };
    }
    return { error: null, contest: contests };
  };
};

const getActiveJoinedContests = (contestData) => {
  return async (user_id) => {
    const contests = await contestData.getCurrentContestsJoined(user_id);

    if (contests.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contests: null,
      };
    }
    return { error: null, contests: contests };
  };
};

const getContestById = (contestData) => {
  return async (id) => {
    const contest = await contestData.getBy('contest_id', id);
    const jury = await contestData.getJuryById(id);

    if (!contest) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null,
      };
    }
    return { error: null, contest: contest, jury: jury };
  };
};

const joinContestByUserId = (contestData) => {
  return async (user_id, contest_id) => {
    const contest = await contestData.getBy('contest_id', contest_id);
    const joinedContest = await contestData.getCurrentContestsJoined(user_id);
    const jury = await contestData.getJuryAndContestById(contest_id, user_id);

    if (joinedContest.some((contest) => contest.contest_id === contest_id)) {
      return { error: serviceErrors.OPERATION_ALREADY_COMPLETED, contest: null };
    }
    if (!contest) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null,
      };
    }
    if (jury) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        contest: null,
      };
    }
    await contestData.addPointsToUser(1, user_id);
    const joinContest = await contestData.joinContest(user_id, contest_id);
    return { error: null, contest: contest };
  };
};
const getJoinedContestByIdandUserId = (contestData) => {
  return async (contest_id, user_id) => {
    const joinedContest = await contestData.getJoinedContestByUserIdAndContestId(
      contest_id,
      user_id,
    );

    if (!joinedContest) {
      return { error: serviceErrors.RECORD_NOT_FOUND, joinedContest: null };
    }
    return { error: null, joinedContest: joinedContest };
  };
};
const createContest = (contestData) => {
  return async (contestCreate) => {
    const { title, jury, invite } = contestCreate;
    const existingContestTitle = await contestData.getBy('title', title);

    if (existingContestTitle) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        contest: null,
      };
    }
    const contest = await contestData.createContest(contestCreate);

    if (jury && jury !== 'Jury') {
      jury.forEach(async (user) => {
        await contestData.selectJury(user.value, contest.contest_id);
      });
    }
    if (invite) {
      contestCreate.invite.forEach(async (user) => {
        await contestData.addPointsToUser(3, user.value);
        await contestData.joinContest(user.value, contest.contest_id);
      });
    }
    return { error: null, newContest: contest };
  };
};

const getFinishedContestsById = (contestData) => {
  return async (user_id) => {
    const contests = await contestData.viewFinishedContestsById(user_id);

    if (!contests) {
      return { error: serviceErrors.RECORD_NOT_FOUND, contests: null };
    }
    return { error: null, contests: contests };
  };
};

const getOpenContestsInPhase1 = (contestData) => {
  return async (user_id) => {
    const contests = await contestData.getOpenContestsinPhase1(user_id);

    if (!contests) {
      return { error: serviceErrors.RECORD_NOT_FOUND, contests: null };
    }
    return { error: null, contests: contests };
  };
};

const getPhaseTimeLimit = (contestData, photosData) => {
  return async (id) => {
    const contest = await contestData.getBy('contest_id', id);
    if (!contest) {
      return { error: serviceErrors.NOT_FOUND, points: null };
    }
    if (contest.status === 'Finished') {
      return { error: null, time: 'null' };
    }

    const dates = await contestData.getPhasesTimeLimit(id);
    const phase1Time = moment(dates.phase1_time_limit);
    const phase2Time = moment(dates.phase2_time_limit);
    const currentTime = moment(new Date());

    const minsAverage = phase1Time.diff(currentTime, 'minutes');
    const minutesUntilPhase2 = parseInt(minsAverage % 60);
    const hoursUntilPhase2 = parseInt(minsAverage / 60);
    const daysUntilPhase2 = parseInt(hoursUntilPhase2 / 24);
    const timeUntilPhase2 = hoursUntilPhase2 - 24 * daysUntilPhase2;

    const minsAveragePhase2 = phase2Time.diff(currentTime, 'minutes');
    const minutesUntilFinished = parseInt(minsAveragePhase2 % 60);
    const hoursUntilFinished = parseInt(minsAveragePhase2 / 60);
    const daysUntilFinished = parseInt(hoursUntilFinished / 24);
    const timeUntilFinished = hoursUntilFinished - 24 * daysUntilFinished;

    const phase1 = daysUntilPhase2 >= 0 && timeUntilPhase2 >= 0 && minutesUntilPhase2 >= 0;
    const phase2 = daysUntilFinished >= 0 && timeUntilFinished >= 0 && minutesUntilFinished >= 0;
  
    if (phase1) {
      return { error: null, time: { daysUntilPhase2, timeUntilPhase2, minutesUntilPhase2 } };
    }

    if (!phase1 && phase2) {
      await contestData.updatePhaseStatus('Phase2', id);
      return {
        error: null,
        time: { daysUntilFinished, timeUntilFinished, minutesUntilFinished },
      };
    }
    
    if (!phase1 && !phase2) {
      await contestData.updatePhaseStatus('Finished', id);
      const jury = await contestData.getJuryById(id);
      const photos = await photosData.getAllPhotos(id);
      jury.forEach(async (jury) => {
        photos.forEach(async (photo) => {
          const reviewedPhotos = await contestData.getReviewdPhotoByJuryIdAndPhotoId(
            jury.user_id,
            photo.photo_id,
          );
          if (reviewedPhotos === undefined) {
            photosData.createPhotoReview({
              photo_id: photo.photo_id,
              score: 3,
              comment: 'Automated',
              wrongCategory: null,
              user_id: jury.user_id,
            });
          }
        });
      });
      return { error: null, time: 'null' };
    }
  };
};

const getAveragePointsByContest = (contestData) => {
  return async (contest_id) => {
    const contest = await contestData.getBy('contest_id', contest_id);
    if (!contest) {
      return { error: serviceErrors.NOT_FOUND, points: null };
    }
    if (contest.status !== 'Finished') {
      return { error: serviceErrors.OPERATION_NOT_PERMITTED, points: null };
    }

    const points = await contestData.getAveragePointsByContest(contest_id);
    points.sort((a, b) => b.average - a.average);
    if (points.length === 0) {
      return { error: serviceErrors.RECORD_NOT_FOUND, points: null };
    }
    if (points[0].place !== null) {
      return { error: serviceErrors.OPERATION_ALREADY_COMPLETED, points: points };
    }

    let userPlace = 1;
    points[0].place = userPlace;
    await contestData.updateUserPlacing(userPlace, points[0].user_id);
    for (let i = 1; i < points.length; i++) {
      if (points[i].average === points[i - 1].average) {
        points[i].place = userPlace;
        await contestData.updateUserPlacing(userPlace, points[i].user_id);
      } else {
        points[i].place = ++userPlace;
        await contestData.updateUserPlacing(userPlace, points[i].user_id);
      }
    }
    await awardContestPoints(points, contestData);
    return { error: null, points: points };
  };
};

const inviteUsersToContest = (contestData) => {
  return async (users, contest_id) => {
    const contest = await contestData.getBy('contest_id', contest_id);
    if (!contest) {
      return { error: serviceErrors.RECORD_NOT_FOUND, contest: null };
    }
    if (contest.type === 'Invitational') {
      users.forEach(async (user) => {
        await contestData.addPointsToUser(3, user.user_id);
        await contestData.joinContest(user.user_id, contest_id);
      });
    }
    if (contest.type !== 'Invitational') {
      return { error: serviceErrors.OPERATION_NOT_PERMITTED, contest: null };
    }
    return { error: null, contest: contest };
  };
};

const selectAdditionalJury = (contestData) => {
  return async (users, contest_id) => {
    const contest = await contestData.getBy('contest_id', contest_id);
    if (!contest) {
      return { error: serviceErrors.RECORD_NOT_FOUND, contest: null };
    }

    if (users.length === undefined) {
      return { error: serviceErrors.DATA_NOT_PROVIDED, contest: null };
    }
    if (users) {
      users.forEach(async (user) => {
        await contestData.selectJury(user.user_id, contest_id);
      });
      return { error: null, contest: contest };
    }
    return { error: null, contest: contest };
  };
};

export default {
  getAllContests,
  getContestById,
  createContest,
  joinContestByUserId,
  getActiveJoinedContests,
  getPhaseTimeLimit,
  getFinishedContestsById,
  getOpenContestsInPhase1,
  getAveragePointsByContest,
  inviteUsersToContest,
  selectAdditionalJury,
  getJoinedContestByIdandUserId,
};

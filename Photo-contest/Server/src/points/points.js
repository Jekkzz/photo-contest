export const awardContestPoints = async (contestPlacing, contestData) => {
  let firstPlacePoints;
  const firstPlaceAveragePoints = contestPlacing.find((contest) => contest.place === 1).average;
  const secondPlaceAveragePoints = contestPlacing.find((contest) => contest.place === 2)?.average;

  const result = contestPlacing.filter(
    (contest) => contest.place === 1 || contest.place === 2 || contest.place === 3,
  );

  secondPlaceAveragePoints ? secondPlaceAveragePoints : 0 * 2 > firstPlaceAveragePoints
    ? (firstPlacePoints = 50)
    : (firstPlacePoints = 75);

  if (contestPlacing.length === 3) {
    contestData.addPointsToUser(firstPlacePoints, contestPlacing[0].user_id);
    contestData.addPointsToUser(35, contestPlacing[1].user_id);
    contestData.addPointsToUser(20, contestPlacing[2].user_id);
  } else {
    const firstPlaceUsers = result.filter((user) => user.place === 1);
    const secondPlaceUsers = result.filter((user) => user.place === 2);
    const thirdPlaceUsers = result.filter((user) => user.place === 3);

    if (firstPlaceUsers.length === 1) {
      contestData.addPointsToUser(firstPlacePoints, firstPlaceUsers[0].user_id);
    } else {
      firstPlaceUsers.forEach((user) => {
        contestData.addPointsToUser(40, user.user_id);
      });
    }

    if (secondPlaceUsers.length === 1) {
      contestData.addPointsToUser(35, secondPlaceUsers[0].user_id);
    } else {
      secondPlaceUsers.forEach((user) => {
        contestData.addPointsToUser(25, user.user_id);
      });
    }
    
    if (thirdPlaceUsers.length === 1) {
      contestData.addPointsToUser(20, thirdPlaceUsers[0].user_id);
    } else {
      thirdPlaceUsers.forEach((user) => {
        contestData.addPointsToUser(10, user.user_id);
      });
    }
  }
};

export const DB_CONFIG = {
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: '12345',
  database: 'photo_contest',
};

export const PORT = 3000;

export const PRIVATE_KEY = 'name';

// 60 mins * 60 secs
export const TOKEN_LIFETIME = 60 * 60;

export const DEFAULT_USER_ROLE = 'Photo Junkie';
export const DEFAULT_USER_RANK = 'Junkie';